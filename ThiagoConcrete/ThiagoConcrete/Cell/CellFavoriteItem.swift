//
//  CellFavoriteItem.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 30/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit
import Reusable

class CellFavoriteItem: UITableViewCell, NibReusable {

    @IBOutlet weak var imgFilm: UIImageView!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup(film: Film) {

        imgFilm.kf.setImage(with: URL(string: film.getImagePath()))

        lblTitle.text = film.title
        lblOverview.text = film.overview
    }
}

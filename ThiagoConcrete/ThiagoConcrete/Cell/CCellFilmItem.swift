//
//  CCellFilmItem.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 10/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher

class CCellFilmItem: UICollectionViewCell, NibReusable {

    @IBOutlet weak var imgPoster: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(film: Film) {

        imgPoster.kf.setImage(with: URL(string: film.getImagePath()))
    }

}

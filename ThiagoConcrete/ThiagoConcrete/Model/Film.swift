//
//  Film.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 10/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class Film: Object {

    @objc dynamic var filmId = ""
    @objc dynamic var video = false
    @objc dynamic var voteAverage = 0.0
    @objc dynamic var title = ""
    @objc dynamic var posterPath = ""
    @objc dynamic var overview = ""
    @objc dynamic var releaseDate: Date?

//    "vote_count": 359,
//    "video": false,
//    "vote_average": 6.8,
//    "title": "Jurassic World: Reino Ameaçado",
//    "popularity": 395.188765,
//    "poster_path": "/nR6LhLKk5BsholuIHIAGemMKPKy.jpg",
//    "genre_ids": [
//    28,
//    12,
//    878
//    ],
//    "backdrop_path": "/t0Kn7twXduHeFhOpTW2Gncu9l5F.jpg",
//    "adult": false,
//    "overview": "Quatro anos após o fechamento do Jurassic Park, um vulcão prestes a entrar em erupção põe em risco a vida na ilha Nublar. No local não há mais qualquer presença humana, com os dinossauros vivendo livremente. Diante da situação, é preciso tomar uma decisão: deve-se retornar à ilha para salvar os animais ou abandoná-los para uma nova extinção? Decidida a resgatá-los, Claire (Bryce Dallas Howard) convoca Owen (Chris Pratt) a retornar à ilha com ela.",
//    "release_date": "2018-06-06"
//    },

    // MARK: - Parse
    static func parseList(_ json: JSON) -> [Film] {
        var array = [Film]()

        for oneElem in json["results"].arrayValue {
            let repo = Film.parseOne(oneElem)
            array.append(repo)
        }

        return array
    }

    static func parseOne(_ json: JSON) -> Film {
        let oneFilm = Film()

        oneFilm.filmId = json["id"].stringValue

        oneFilm.posterPath = json["poster_path"].stringValue
        oneFilm.title = json["title"].stringValue
        oneFilm.overview = json["overview"].stringValue

        if let releaseDate = json["release_date"].string {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            oneFilm.releaseDate = formatter.date(from: releaseDate)
        }

        return oneFilm
    }

    func getImagePath() -> String {
        return "http://image.tmdb.org/t/p/w185\(posterPath)"
    }

    func removeFavorite() -> Void {

        let realm = try! Realm()

        try! realm.write {
            realm.delete(self)
        }
    }
}

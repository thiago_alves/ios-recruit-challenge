//
//  ViewState.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 01/07/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit

enum State {
    case loading
    case error (string: String)
}

class ViewState: UIView {

    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var lblMessage: UILabel!

    var view: UIView!

    // MARK: - Initialization
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        xibSetup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    // MARK: - Configuration
    func xibSetup() {
        view = self.loadNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }

    // MARK: - Methods
    func setup (state: State){

        viewLoading.isHidden = true
        viewMessage.isHidden = true

        switch state {
        case .loading:
            viewLoading.isHidden = false
            indicator.startAnimating()
        case .error(string: let message):
            viewMessage.isHidden = false
            lblMessage.text = message
        }
    }
}

//
//  UIView.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 01/07/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit

extension UIView {

    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}

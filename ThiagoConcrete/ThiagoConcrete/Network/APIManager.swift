//
//  APIManager.swift
//  GitHubThiago
//
//  Created by Thiago Alves on 05/11/2017.
//  Copyright © 2017 Thiago Alves. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

public protocol TargetType {
    
    /// The target's base `URL`.
    var baseURL: String { get }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }
    
    /// The HTTP method used in the request.
    var method: Alamofire.HTTPMethod { get }
    
    /// The parameters to be incoded in the request.
    var parameters: [String: Any]? { get }
}

enum API {
    
    // MARK: Main
    case popularFilms(page: Int)
}


extension API: TargetType {


    /// The target's base `URL`.
    var baseURL: String { return "https://api.themoviedb.org/3/" }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
        case .popularFilms:
            return "movie/popular"
        }
    }
    
    /// The HTTP method used in the request.
    var method: Alamofire.HTTPMethod {
        switch self {
        case .popularFilms:
            return .get

        }
    }
    
    /// The parameters to be incoded in the request.
    var parameters: [String: Any]? {
        switch self {
        case .popularFilms(page: let page):
            return ["page": page, "api_key": "0ebe933f7358aea29ca3ccccf97f582a", "language": "pt-BR"]
        }
    }
    
    /// The method used for parameter encoding.
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .popularFilms:
            return URLEncoding.default
        }
    }
}

class APIManager: NSObject {

    typealias JsonObject = [String: Any]

    enum Result<T> {
        case success(T)
        case error
    }

    static func request(_ endPoint: API, completion: @escaping (Result<JSON>) -> Void) {

        Alamofire.request(endPoint.baseURL + endPoint.path,
                          method: endPoint.method,
                          parameters: endPoint.parameters,
                          encoding: endPoint.parameterEncoding,
                          headers: nil).responseJSON { (response) in

                            print(response)
                            if let value = response.result.value {
                                let result = JSON(value)
                                completion(.success(result))

                            } else {
                                completion(.error)
                            }
        }
    }
}

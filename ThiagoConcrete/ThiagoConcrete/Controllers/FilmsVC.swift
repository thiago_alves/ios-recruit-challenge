//
//  FilmsVC.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 10/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit
import Hero

class FilmsVC: UIViewController {

    @IBOutlet weak var viewState: ViewState!
    @IBOutlet weak var collectionView: UICollectionView!

    var tableDatasource: FilmListDatasource?
    var tableDelegate: FilmListDelegate?

    var listFilms: [Film] = []

    // Pagination
    var indexOfPageToRequest = 1
    var isLoadingFromServer = false
    var reachedListEnd = false

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.hero.isEnabled = true
        navigationController?.hero.navigationAnimationType = .selectBy(presenting: .zoomSlide(direction: .left), dismissing: .zoomSlide(direction: .right))

        wsGetMoviesList(page: indexOfPageToRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Fileprivate Methods
    fileprivate func wsGetMoviesList(page: Int) {

        viewState.isHidden = false
        viewState.setup(state: .loading)

        isLoadingFromServer = true

        APIManager.request(.popularFilms(page: page)) { (results) in

            switch results {
            case .success(let json):

                self.viewState.isHidden = true

                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    // Do not flood the server
                    self.isLoadingFromServer = false
                }

                let list = Film.parseList(json)

                if list.count != 20 {
                    self.reachedListEnd = true
                }

                self.listFilms += list

                self.tableDelegate = FilmListDelegate(self)
                self.tableDatasource = FilmListDatasource(items: self.listFilms,
                                                          collectionView: self.collectionView,
                                                          delegate: self.tableDelegate!)
            case .error:
                self.viewState.setup(state: .error(string: "Ocorreu um erro\nTente novamente mais tarde"))
            }
        }
    }
}

extension FilmsVC: CollectionDelegate {
    func didSelect(at index: IndexPath) {

        let detailVC: FilmDetailVC = UIStoryboard(storyboard: .filmDetail).instantiateViewController()
        detailVC.film = listFilms[index.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }

    func incrementPage() {

        if isLoadingFromServer || reachedListEnd {
            return
        }

        // call your API for more data
        indexOfPageToRequest += 1
        wsGetMoviesList(page: indexOfPageToRequest)

    }
}

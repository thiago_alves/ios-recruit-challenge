//
//  FavoritesVC.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 30/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit
import RealmSwift

class FavoritesVC: UIViewController {

    @IBOutlet weak var viewState: ViewState!

    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var table: UITableView!

    var listFilms: [Film] = [] {
        didSet {

            if listFilms.isEmpty {
                viewState.isHidden = false
                if isSearching {
                    viewState.setup(state: .error(string: "Não encontramos nenhum resultado para a sua busca"))
                } else {
                    viewState.setup(state: .error(string: "Você ainda não possui nenhum favorito"))
                }
            } else {
                viewState.isHidden = true
                self.tableDelegate = FavoriteDelegateTB(self)
                self.tableDatasource = FavoriteDataSourceTB(items: listFilms, tableView: table, delegate: tableDelegate!)
            }
        }
    }

    var tableDatasource: FavoriteDataSourceTB?
    var tableDelegate: FavoriteDelegateTB?

    var isSearching = false {
        didSet{
            if isSearching {

            } else {

            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        table.register(cellType: CellFavoriteItem.self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        getFavorites()
    }

    // MAR: - Methods
    fileprivate func getFavorites() {

        let realm = try! Realm()

        let list = realm.objects(Film.self)

        listFilms = Array(list)
    }
}

extension FavoritesVC: TableDelegate {

    func remove(at index: IndexPath) {

        listFilms[index.row].removeFavorite()
        listFilms.remove(at: index.row)
    }

    func didSelect(at index: IndexPath) {

        let detailVC: FilmDetailVC = UIStoryboard(storyboard: .filmDetail).instantiateViewController()
        detailVC.film = listFilms[index.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension FavoritesVC: UISearchBarDelegate {

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

        isSearching = true
        searchBar.showsCancelButton = true
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        isSearching = false

        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)

        getFavorites()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchText != "" {

            let realm = try! Realm()
            let list = realm.objects(Film.self).filter({ $0.title.lowercased().contains(searchText.lowercased())})
            listFilms = Array(list)

        } else {
            getFavorites()
        }
    }
}

//
//  FilmDetailVC.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 30/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit
import RealmSwift

class FilmDetailVC: UIViewController {

    @IBOutlet weak var imgPosterFilm: UIImageView!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOverview: UILabel!

    @IBOutlet weak var btnFavorite: UIButton!

    @IBOutlet weak var lblDate: UILabel!

    var film: Film?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.hero.isEnabled = true
        navigationController?.hero.navigationAnimationType = .selectBy(presenting: .zoomSlide(direction: .left), dismissing: .zoomSlide(direction: .right))

        setupFilm()
    }

    fileprivate func setupFilm() {

        guard let film = film else {
            return
        }

        let realm = try! Realm()

        let savedElem = realm.objects(Film.self).filter ({ $0.filmId == film.filmId })

        setFavoriteButton(isFavorite: savedElem.count > 0)

        imgPosterFilm.kf.setImage(with: URL(string: film.getImagePath()))

        lblName.text = film.title
        lblOverview.text = film.overview

        if let date = film.releaseDate {
            lblDate.text = "\(date.year)"
        } else {
            lblDate.text = ""
        }

        self.title = film.title
    }

    fileprivate func setFavoriteButton(isFavorite: Bool) {

        let image = isFavorite ? UIImage(named: "favorite_full_icon"): UIImage(named: "favorite_empty_icon")
        btnFavorite.setImage(image, for: .normal)

    }

    // MARK: - Action
    @IBAction func setFavorite(_ sender: UIButton) {

        guard let film = film else {
            return
        }

        let realm = try! Realm()
        let savedElem = realm.objects(Film.self).filter ({ $0.filmId == film.filmId })

        if let elem = savedElem.first {
            try! realm.write {
                realm.delete(elem)
            }
            setFavoriteButton(isFavorite: false)
        } else {
            try! realm.write {
                realm.add(film)
            }
            setFavoriteButton(isFavorite: true)
        }

    }
}

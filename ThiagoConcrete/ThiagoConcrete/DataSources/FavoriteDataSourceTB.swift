//
//  FavoriteListTB.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 30/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit
import RealmSwift

protocol TableDelegate {
    func didSelect(at index: IndexPath)
    func remove(at index: IndexPath)
}

class FavoriteDataSourceTB: NSObject {

    var items: [Film] = []
    weak var tableView: UITableView?
    weak var delegate: UITableViewDelegate?

    required init(items: [Film], tableView: UITableView, delegate: UITableViewDelegate) {
        self.items = items
        self.tableView = tableView
        self.delegate = delegate
        super.init()
        tableView.register(cellType: CellFavoriteItem.self)
        self.setupTableView()
    }

    func setupTableView() {
        self.tableView?.dataSource = self
        self.tableView?.delegate = self.delegate
        self.tableView?.tableFooterView = UIView()

        self.tableView?.reloadData()
    }
}

extension FavoriteDataSourceTB: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell: CellFavoriteItem = tableView.dequeueReusableCell(for: indexPath)
        cell.setup(film: items[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

class FavoriteDelegateTB: NSObject, UITableViewDelegate {

    let delegate: TableDelegate

    init(_ delegate: TableDelegate) {
        self.delegate = delegate
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let remove = UITableViewRowAction(style: .destructive, title: "Desfavoritar") { (action, index) in
            self.delegate.remove(at: index)
        }

        return [remove]
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didSelect(at: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

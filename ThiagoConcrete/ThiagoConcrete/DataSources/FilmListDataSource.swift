//
//  FilmListDataSource.swift
//  ThiagoConcrete
//
//  Created by Thiago Alves on 10/06/2018.
//  Copyright © 2018 Thiago Alves. All rights reserved.
//

import UIKit

protocol CollectionDelegate {
    func didSelect(at index: IndexPath)
    func incrementPage()
}

class FilmListDatasource: NSObject {

    weak var collectionView: UICollectionView?
    weak var delegate: UICollectionViewDelegate?

    var items: [Film] = []

    required init(items: [Film], collectionView: UICollectionView, delegate: UICollectionViewDelegate) {
        self.items = items
        self.collectionView = collectionView
        self.delegate = delegate
        super.init()
        collectionView.register(cellType: CCellFilmItem.self)
        self.setupTableView()
    }

    func setupTableView() {
        self.collectionView?.dataSource = self
        self.collectionView?.delegate = self.delegate
        self.collectionView?.reloadData()
    }
}

extension FilmListDatasource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CCellFilmItem = collectionView.dequeueReusableCell(for: indexPath)
        cell.setup(film: items[indexPath.row])
        return cell
    }
}

class FilmListDelegate: NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    let delegate: CollectionDelegate

    init(_ delegate: CollectionDelegate) {
        self.delegate = delegate
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellWidth =  (UIScreen.main.bounds.width / 2) - 36
        return CGSize(width: cellWidth, height: cellWidth * 1.5)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.didSelect(at: indexPath)
    }
}

extension FilmListDelegate: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if (offsetY + 700.0) > (contentHeight - scrollView.frame.size.height) {
            delegate.incrementPage()
        }
    }
}
